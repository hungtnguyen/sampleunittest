package app.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import app.utils;


/**
 * Created by hungthanhnguyen on 12/14/16.
 */
public class demoTest {
    @Test
    public void TestAdd() {
        int a = 10;
        int b = 10;
        int total = a + b;
        utils utils = new utils();
        Assert.assertTrue(utils.add(a,b) == total, String.format("%s + %s is not equal %s", a,b,total));
    }

    @Test
    public void TestSubstract() {
        int a = 10;
        int b = 10;
        int total = a - b;
        utils utils = new utils();
        Assert.assertTrue(utils.substract(a, b) == total, String.format("%s - %s is not equal %s", a,b,total));
    }

    @Test
    public void TestMultiple() {
        int a = 10;
        int b = 10;
        int total = a * b;
        utils utils = new utils();
        Assert.assertTrue(utils.multiple(a, b) == total, String.format("%s * %s is not equal %s", a,b,total));
    }

    @Test
    public void TestDevide() {
        int a = 10;
        int b = 10;
        double total = a / b;
        utils utils = new utils();
        Assert.assertTrue(utils.devide(a,b) == total, String.format("%s * %s is not equal %s", a,b,total));
    }
}
